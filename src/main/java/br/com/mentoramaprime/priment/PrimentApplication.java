package br.com.mentoramaprime.priment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrimentApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrimentApplication.class, args);
	}

}
