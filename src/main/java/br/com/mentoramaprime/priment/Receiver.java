package br.com.mentoramaprime.priment;

import br.com.mentoramaprime.priment.vos.RatingVO;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Receiver {

    @JmsListener(destination = "mailbox",containerFactory = "myFactory")
    public void message(RatingVO avaliacaoVO){
        if (avaliacaoVO.getNome_filme() == null || avaliacaoVO.getNota() == null || avaliacaoVO.getComentario() == null){
            throw new RuntimeException("Invalid rating");
        }
        System.out.println("Success sent rating");
    }

}
