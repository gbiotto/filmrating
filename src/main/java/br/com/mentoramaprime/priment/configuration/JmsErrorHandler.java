package br.com.mentoramaprime.priment.configuration;

import org.springframework.util.ErrorHandler;

public class JmsErrorHandler implements ErrorHandler {

    @Override
    public void handleError(Throwable throwable){
        System.out.println("Error processing rating " + throwable.getMessage());
    }
}
