package br.com.mentoramaprime.priment.controller;

import br.com.mentoramaprime.priment.entities.RatingEntity;
import br.com.mentoramaprime.priment.repository.RatingRepository;
import br.com.mentoramaprime.priment.vos.RatingVO;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rating")
public class RatingController {

    private final RatingRepository ratingRepository;

    public RatingController(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    @GetMapping
    public List<RatingVO> findAll(@RequestParam("page") Integer page,
                                  @RequestParam("pageSize") Integer pageSize){
        return this.ratingRepository.findAll(PageRequest.of(page, pageSize,
                Sort.by("nome_filme")))
                .stream()
                .map(this::toRatingVO)
                .collect(Collectors.toList());
    }

    private RatingVO toRatingVO(RatingEntity rating) {
        RatingVO ratingVO = new RatingVO();
        ratingVO.setNome_filme(ratingVO.getNome_filme());
        ratingVO.setComentario(ratingVO.getComentario());
        ratingVO.setNota(ratingVO.getNota());
        ratingVO.setId_usuario(ratingVO.getId_usuario());
        return  ratingVO;
    }

    @GetMapping("/{nota}")
    public List<RatingEntity> findFilmeByNota(@PathVariable("nota") final Double nota){
        return this.ratingRepository.findFilmeByNota(nota);
    }

    @PostMapping
    public void RatingFilm(@RequestBody final RatingEntity rating){
        rating.setNome_filme(rating.getNome_filme());
        rating.setNota(rating.getNota());
        rating.setComentario(rating.getComentario());
        rating.setId_usuario(rating.getId_usuario());
    }

}
