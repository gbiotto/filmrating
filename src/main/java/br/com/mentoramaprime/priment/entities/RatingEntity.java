package br.com.mentoramaprime.priment.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "avaliacao")
public class RatingEntity {

    @Column(name = "nome_filme", nullable = false)
    private String nome_filme;

    @Column(name = "nota", nullable = false)
    private Integer nota;

    @Column(name = "comentario", nullable = false)
    private String comentario;

    @Column(name = "id_usuario", nullable = false)
    private Integer id_usuario;

    public RatingEntity(String nome_filme, Integer nota, String comentario, Integer id_usuario) {
        this.nome_filme = nome_filme;
        this.nota = nota;
        this.comentario = comentario;
        this.id_usuario = id_usuario;
    }

    public String getNome_filme() {
        return nome_filme;
    }

    public void setNome_filme(String nome_filme) {
        this.nome_filme = nome_filme;
    }

    public Integer getNota() {
        return nota;
    }

    public void setNota(Integer nota) {
        this.nota = nota;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Integer getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Integer id_usuario) {
        this.id_usuario = id_usuario;
    }
}
