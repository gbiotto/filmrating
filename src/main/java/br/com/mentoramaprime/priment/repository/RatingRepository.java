package br.com.mentoramaprime.priment.repository;

import br.com.mentoramaprime.priment.entities.RatingEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface RatingRepository extends JpaRepository<RatingEntity, Long> {

    @Query(value = "select a from RatingEntity a left join a.nome_filme;", nativeQuery = true)
    List<RatingEntity> findByFilme(String nome_filme);

    @Query(value = "Select a from RatingEntity where  a.nota = ?1;", nativeQuery = true)
    List<RatingEntity> findFilmeByNota(Double nota);

}
