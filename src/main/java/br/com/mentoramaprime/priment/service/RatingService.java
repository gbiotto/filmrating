package br.com.mentoramaprime.priment.service;

import br.com.mentoramaprime.priment.entities.RatingEntity;
import br.com.mentoramaprime.priment.repository.RatingRepository;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class RatingService {

    private final RatingRepository ratingRepository;

    public RatingService(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    @Transactional
    public List<RatingEntity>findAll(){
        return this.ratingRepository.findAll();
    }

}
