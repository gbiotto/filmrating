package br.com.mentoramaprime.priment.vos;

public class RatingVO {

    private String nome_filme;
    private Integer nota;
    private String comentario;
    private Integer id_usuario;

    public String getNome_filme() {
        return nome_filme;
    }

    public void setNome_filme(String nome_filme) {
        this.nome_filme = nome_filme;
    }

    public Integer getNota() {
        return nota;
    }

    public void setNota(Integer nota) {
        this.nota = nota;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Integer getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Integer id_usuario) {
        this.id_usuario = id_usuario;
    }

    @Override
    public String toString(){
        return String.format(
                "Filme=%s, " +
                "Nota=%s, " +
                "Comentario=%s, " +
                "Id Usuário=%s}",
                getNome_filme(), getNota(), getComentario(), getId_usuario());
    }

}
