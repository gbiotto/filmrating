-- DROP SCHEMA public;

CREATE SCHEMA public AUTHORIZATION postgres;

COMMENT ON SCHEMA public IS 'standard public schema';
-- public.avaliacao definition

-- Drop table

-- DROP TABLE public.avaliacao;

CREATE TABLE public.avaliacao (
	nome_filme varchar(200) NULL,
	nota int4 NULL,
	comentario varchar(800) NULL,
	id_usuario int4 NULL
);